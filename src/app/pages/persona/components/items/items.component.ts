import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {  
  constructor(public dialog: MatDialog,) { }
  @Input('content') content: any

  ngOnInit(): void {
  }
  
  openDialogInfo(info: any) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '450px',
      data: info,
    });
  }
}
