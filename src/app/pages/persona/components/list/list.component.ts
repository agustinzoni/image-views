import { Component, OnInit } from '@angular/core';
import { PersonaService } from '../../service/persona.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  informations = []
  constructor(private persona: PersonaService, public dialog: MatDialog,) { }

  ngOnInit(): void {
    this.persona.getPersona().subscribe((persona) => {
      this.informations = persona.results
    })
  }

  openDialogInfo() {
    const dialogRef = this.dialog.open(DialogComponent, {
      // width: '100%',
      // height: '100%',
      width: '270px',
      data: '',
    });

    dialogRef.afterClosed().subscribe();
  }
}

