import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaRoutingModule } from './persona-routing.module';
import { PersonaComponent } from './persona.component';
import { ListComponent } from './components/list/list.component';
import { ItemsComponent } from './components/items/items.component';
import { MaterialModule } from '@app/material.module';
import { SharedModule } from '@app/shared/shared.module';
import { DialogComponent } from './components/dialog/dialog.component';


@NgModule({
  declarations: [
    PersonaComponent,
    ListComponent,
    ItemsComponent,
    DialogComponent
  ],
  imports: [
    CommonModule,
    PersonaRoutingModule,
    MaterialModule,
    SharedModule
  ]
})
export class PersonaModule { }
