import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

// export interface Iresult{
//   persona: IPersona
// }
// export interface IPersona{
//   cell: string,
//   gender: string,
//   email: string,
//   name : Iname,
//   location: Ilocation,
//   dob: Idob,
//   imagen: Ipicture,

// }
// export interface Iname{
//   first: string,
//   last: string,
//   title: string,
// }
// export interface Idob{
//   age: number,
//   date: string
// }
// export interface Ilocation{
//   city: string,
//   country: string,
//   postcode: number,
//   state: string
// }
// export interface Ipicture{
//   large: string,
//   medium: string,
//   thumbnail: string,
// }
@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  personaUrl = environment.API_URL;

  constructor(private http: HttpClient) { }

  getPersona(): Observable<any>{
    // console.log(this.http.get<IPersona>(`${this.personaUrl}/api`), {result: 16});
    return this.http.get<any>(`${this.personaUrl}`)
  }
}
