import { Component, OnInit } from '@angular/core';
import { PersonaService } from './service/persona.service';

@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.scss']
})
export class PersonaComponent implements OnInit {
  constructor(private persona: PersonaService) { }

  ngOnInit(): void {
  }
}
