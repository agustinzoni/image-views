import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './component/footer/footer.component';
import { HeaderComponent } from './component/header/header.component';
import { LateralMenuComponent } from './layouts/lateral-menu/lateral-menu.component';
import { MaterialModule } from '@app/material.module';



@NgModule({
  declarations: [FooterComponent, HeaderComponent, LateralMenuComponent],
  imports: [
    CommonModule, MaterialModule
  ],
  exports: [ FooterComponent, HeaderComponent, LateralMenuComponent ]
})
export class SharedModule { }
