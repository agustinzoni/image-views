import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private _openEndSideNav = false;
  isHandset: boolean = true;
  showLoader: boolean = false;
  routes = [
    {
      name: 'Inicio',
      path: '/dashboard/panel',
      icon: '../../assets/img/icons/panel.png',
    },
    {
      name: 'Recibos',
      path: '/dashboard/recibos',
      icon: '../../assets/img/icons/recibos.png',
    },
    {
      name: 'Legajos',
      path: '/dashboard/legajos',
      icon: '../../assets/img/icons/legajo.png',
    },
    {
      name: 'Trámites',
      path: '/dashboard/tramites',
      icon: '../../assets/img/icons/tramites.png',
    },
    {
      name: 'Enterate',
      path: '/dashboard/info',
      icon: '../../assets/img/icons/enterate.png',
    },
    {
      name: 'IFCAM',
      path: '/dashboard/ifcam',
      icon: '../../assets/img/icons/ifcam.png',
    },
  ];
  
  constructor(public cd: ChangeDetectorRef,) { }

  ngOnInit(): void {
  }

  set openEndSideNav(o: boolean) {
    this._openEndSideNav = o;
    this.cd.markForCheck();
  }
  get openEndSideNav() {
    return this._openEndSideNav;
  }

}
