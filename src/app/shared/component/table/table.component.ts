import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  title:string = 'Personas'
  dataSource = []
  constructor() { }

  ngOnInit(): void {
  }

}
