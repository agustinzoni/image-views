import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

  @Input() newsletter = false;

  // get author(): string {
  //   return pkg.author.name;
  // }

  // get emailAuthor(): string {
    // return pkg.author.email;
  // }
  get license(): string {
    return '';
    // return pkg.license;
  }
  get repository(): string {
    return '';
    // return pkg.repository.url;
  }

  constructor() {}

  ngOnInit() {}

  onSubmit() {
    // console.dir(this.inputEmail);
  }
}
