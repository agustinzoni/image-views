import { NgModule } from "@angular/core";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list'
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';


const myModules: any = [ MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule, MatDialogModule, MatButtonModule];
@NgModule({
    imports: [...myModules],
    exports: [...myModules]
})
export class MaterialModule {}